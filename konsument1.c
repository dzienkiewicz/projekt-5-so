#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <errno.h>
#include <sys/shm.h>
int id_pamiec;
char *address;
int semafor;
#define true (1==1)

void pd_create(int isProducent);
char * pd_address();
void pd_detach(int onlyD2);
void semafor_create(void);
void semafor_set(void);
void semafor_wait(unsigned short sem_number);
void semafor_signal(unsigned short sem_number);
void semafor_delete(void);
void semafor_access_child(void);
int finish(int CODE);

int main(){
	printf("PROCES KONSUMENT: PID=%d\n", getpid());
	semafor_access_child();

	FILE *output_file;
	char ch = '0';

	pd_create(0);
	pd_address();

	output_file = fopen("wynik.txt", "w");
	if (!output_file){
		perror("FILE (w) error");
		pd_detach(0);
		semafor_delete();
		finish(-1);
	}
	else{
		do{
			sleep(3);			
			semafor_wait(1);
			ch = *address;
			if (ch != EOF){
				fputc(ch, output_file);
				if (ch == 10)
					printf("KONSUMENT(%d)-pisz do pliku=(##EDNL##)\n", ch);
				else if (ch == 32)
					printf("KONSUMENT(%d)-pisz do pliku=(##SPACJA##)\n", ch);
				else
					printf("KONSUMENT:(%d)-pisz do pliku=(%c)\n", ch, ch);
			sleep(3);						
			semafor_signal(0);
			}
		} while (ch != EOF);
	}
	if (ch == EOF){
		semafor_wait(1);
		fclose(output_file);
		semafor_signal(0);
		shmdt(address);
		shmctl(id_pamiec, IPC_RMID, 0);
	}
	semafor_delete();
	printf("KONSUMENT-koniec programu\n");
	return 0;

}
void pd_create(int isProducent){
	key_t klucz = ftok("../../pazaczkiewicz/projektprod", 'R');
	if (klucz == -1){
		perror("ftok error\n");
		finish(-1);
	}
	else{
		if (isProducent == 1){
			printf("PRODUCENT shmget:");
			id_pamiec = shmget(klucz, sizeof(char), 0660 | IPC_CREAT);
		}
		else{
			printf("KONSUMENT shmget:");
			id_pamiec = shmget(klucz, sizeof(char), 0660 | IPC_CREAT);
		}
	}
	if (id_pamiec == -1){
		printf("error=%d\n", errno);
		perror("SHMGET error:");
		finish(-1);
	}
	else{
		printf("Utworzono segment pamięci : %d\n", id_pamiec);
	}
};

char *pd_address(){
	char *addresss;
	addresss = (char *) shmat(id_pamiec, 0, 0);
	if (addresss == (char *) -1){
		perror("SHMAT error");
		finish(-1);
	}
	else{
		printf("Uzyskano adres %p segmentu pamięci : %d\n", &addresss, id_pamiec);
	}
	address = addresss;
	return addresss;
};

void pd_detach(int onlyD2){
	int d1, d2;
	if (!onlyD2){
		d1 = shmctl(id_pamiec, IPC_RMID, 0);
		d2 = shmdt(address);
		if (d1 == -1 || d2 == -1){
			perror("SHMCTL || SHMDT error");
			finish(-1);
		}
		else{
			printf("Odłączono pamięć dzieloną : %d\n", id_pamiec);
		}
	}
	else{
		d2 = shmdt(address);
		if (d2 == -1){
			perror("SHMCTL || SHMDT error");
			finish(-1);
		}
		else{
			printf("Odłączono pamięć dzieloną : %d\n", id_pamiec);
		}
	}
};

void semafor_create(void){
	key_t klucz = ftok("../../pazaczkiewicz/projektprod", 'Z');
	if (klucz == -1){
		perror("ftok error\n");
		exit(-1);
	}
	else
		semafor = semget(klucz, 2, 0660 | IPC_CREAT | IPC_EXCL);
	if (semafor == -1){
		printf("SEMAFOR_CREATE error\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("Semafor zostal utworzony : %d\n", semafor);
	}
}

void semafor_access_child(void){
	key_t klucz = ftok("../../pazaczkiewicz/projektprod", 'Z');
	if (klucz == -1){
		perror("ftok error\n");
		exit(-1);
	}
	else

		semafor = semget(klucz, 1, 0660 | IPC_CREAT);
	if (semafor == -1){
		printf("SEMAFOR_JOIN_CHILD error\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("Semafor zostal odnaleziony i uzyskal dostep : %d\n", semafor);
	}
}

void semafor_set(void){
	int ustaw_sem;
	unsigned short array[2] = {1, 0};
	ustaw_sem = semctl(semafor, 0, SETALL, array);
	if (ustaw_sem == -1){
		printf("SEMAFOR_SET error\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("Semafory zostaly ustawione.\n");
	}
}


void semafor_wait(unsigned short sem_number){//semafor_p
	int zmien_sem;
	struct sembuf bufor_sem;
	bufor_sem.sem_num = sem_number;
	bufor_sem.sem_op = -1;
	bufor_sem.sem_flg = 0;
	while (true){
		zmien_sem = semop(semafor, &bufor_sem, 1);
		if (zmien_sem == 0 || errno != 4)
			break;
	}
	if (zmien_sem == -1 && errno != 4){
		perror("error -> ");
		printf("SEMAFOR_CLOSE error\n");
		semafor_delete();
		exit(EXIT_FAILURE);
	}
	else{
		int stan = semctl(semafor, 0, GETVAL);
		if (stan != -1){
		}
	}
}

void semafor_signal(unsigned short sem_number){//semafor_v
	int zmien_sem;
	struct sembuf bufor_sem;
	bufor_sem.sem_num = sem_number;
	bufor_sem.sem_op = 1;
	bufor_sem.sem_flg = 0;
	while (true){
		zmien_sem = semop(semafor, &bufor_sem, 1);
		if (zmien_sem == 0 || errno != 4)
			break;
	}
	if (zmien_sem == -1 && errno != 4){
		printf("SEMAFOR_OPEN error\n");
		semafor_delete();
		exit(EXIT_FAILURE);
	}
	else{
		int stan = semctl(semafor, 0, GETVAL);
		if (stan != -1){
		}
	}
}

void semafor_delete(void){
	int sem;
	sem = semctl(semafor, 0, IPC_RMID);
	if (sem == -1){
		printf("SEMAFOR_DELETE error\n");
		exit(EXIT_FAILURE);
	}
	else{
		printf("Semafory zostały usuniete kod:%d\n", sem);
	}
}

int finish(int CODE){
	perror("FINISHED");
	exit(CODE);
}
